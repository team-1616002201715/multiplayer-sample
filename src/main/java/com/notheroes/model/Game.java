package com.notheroes.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.Map;

@Data
public class Game {

    private String gameId;
    private Player player1;
    private Player player2;
    private ArrayList<Unit> units = new ArrayList<Unit>();
    private GameStatus status;
}
