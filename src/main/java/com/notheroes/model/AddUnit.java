package com.notheroes.model;

import lombok.Data;

@Data
public class AddUnit {
    private Unit unit;
    private String gameId;
}
