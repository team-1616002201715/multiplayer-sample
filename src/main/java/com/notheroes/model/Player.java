package com.notheroes.model;

import lombok.Data;

@Data
public class Player {
    private String login;
}
