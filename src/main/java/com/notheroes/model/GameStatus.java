package com.notheroes.model;

public enum GameStatus {
    OPEN, IN_PROGRESS, FINISHED
}
