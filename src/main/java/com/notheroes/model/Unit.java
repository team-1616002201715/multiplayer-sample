package com.notheroes.model;

import lombok.Data;

@Data
public class Unit {
    private String name;
    private String owner;
    private int hp;
}
