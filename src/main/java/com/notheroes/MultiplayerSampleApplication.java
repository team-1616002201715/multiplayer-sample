package com.notheroes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiplayerSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultiplayerSampleApplication.class, args);
	}

}
