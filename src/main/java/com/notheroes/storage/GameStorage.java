package com.notheroes.storage;

import com.notheroes.model.Game;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.notheroes.model.GameStatus.*;

@Service
public class GameStorage {

    private static Map<String, Game> games;
    private static GameStorage instance;

    private GameStorage() {
        games = new HashMap<>();
    }

    public static synchronized GameStorage getInstance() {
        if (instance == null) {
            instance = new GameStorage();
        }
        return instance;
    }

    public Map<String, Game> getGames() {
        return games;
    }

    public Set<String> getGamesById(){
        return games.keySet();
    }

    public Set<String> getOpenGames(){
        return games.values().stream().filter(it -> it.getStatus().equals(OPEN)).map(Game::getGameId).collect(Collectors.toSet());
    }

    public void setGame(Game game) {
        games.put(game.getGameId(), game);
    }
}