package com.notheroes.controller;

import com.notheroes.model.AddUnit;
import com.notheroes.model.Game;
import com.notheroes.model.Player;
import com.notheroes.service.GameService;
import com.notheroes.storage.GameStorage;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@AllArgsConstructor
@RequestMapping("/game")
@CrossOrigin(origins = "*")

public class GameController {

    private final GameService gameService;
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final GameStorage gameStorage;

    @PostMapping("/start")
    public ResponseEntity<Game> start(@RequestBody Player player) {
        log.info("start game request: {}", player);
        Game game = gameService.createGame(player);
        simpMessagingTemplate.convertAndSend("/topic/rooms/", gameStorage.getOpenGames());
        return ResponseEntity.ok(game);
    }

    @PostMapping("/connect")
    public ResponseEntity<Game> connectToGameId(@RequestBody Player player, String gameId){
        log.info("Join room request: {}", gameId);
        Game game = gameService.connectToGameId(player, gameId);
        simpMessagingTemplate.convertAndSend("/topic/rooms/", gameStorage.getOpenGames());
        return ResponseEntity.ok(game);
    }

    @PostMapping("/connect/random")
    public ResponseEntity<Game> connectRandom(@RequestBody Player player){
        log.info("connect random {}", player);
        Game game = gameService.connectToRandomGame(player);
        simpMessagingTemplate.convertAndSend("/topic/rooms/", gameStorage.getOpenGames());
        return ResponseEntity.ok(game);
    }

    @PostMapping("/gameplay")
    public ResponseEntity<Game> addUnit(@RequestBody AddUnit request){
        log.info("gameplay: {}", request);
        Game game = gameService.addUnit(request);
        simpMessagingTemplate.convertAndSend("/topic/gameplay/" + game.getGameId(), game);
        return ResponseEntity.ok(game);
    }
}
