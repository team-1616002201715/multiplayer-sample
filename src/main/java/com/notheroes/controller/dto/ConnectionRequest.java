package com.notheroes.controller.dto;

import com.notheroes.model.Player;
import lombok.Data;

@Data
public class ConnectionRequest {
    private Player player;
    private String gameId;
}
