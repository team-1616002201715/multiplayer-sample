package com.notheroes.service;

import com.notheroes.model.*;
import com.notheroes.storage.GameStorage;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

import static com.notheroes.model.GameStatus.*;

@Service
@AllArgsConstructor
public class GameService {

    private final GameStorage gameStorage = GameStorage.getInstance();

    public Game createGame(Player player){
        Game game = new Game();
        game.setGameId(UUID.randomUUID().toString());
        game.setPlayer1(player);
        game.setStatus(OPEN);
        gameStorage.setGame(game);
        return game;
    }

    public Game connectToGameId(Player player2, String gameId){
        Game game = gameStorage.getGames().values().stream()
                .filter(it -> it.getGameId().equals(gameId))
                .findFirst().orElseThrow(() -> new NullPointerException("Game not found"));
        game.setPlayer2(player2);
        game.setStatus(IN_PROGRESS);
        gameStorage.setGame(game);
        return game;
    }

    public Game connectToRandomGame(Player player2){
        Game game = gameStorage.getGames().values().stream()
                .filter(it -> it.getStatus().equals(OPEN))
                .findFirst().orElseThrow(() -> new NullPointerException("Game not found"));
        game.setPlayer2(player2);
        game.setStatus(IN_PROGRESS);
        gameStorage.setGame(game);
        return game;
    }

    public Game addUnit(AddUnit addUnit){
        if (!GameStorage.getInstance().getGames().containsKey(addUnit.getGameId())) {
            throw new NullPointerException("Game not found");
        }

        Game game = GameStorage.getInstance().getGames().get(addUnit.getGameId());
        if (game.getStatus().equals(FINISHED)) {
            throw new NullPointerException("Game is already finished");
        }

        ArrayList<Unit> units = game.getUnits();
        units.add(addUnit.getUnit());
        game.setUnits(units);
        gameStorage.setGame(game);
        return game;
    }
}
