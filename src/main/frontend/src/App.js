import './App.css';
import SockJS from "sockjs-client"
import Stomp from "stompjs"
import axios from "axios"
let stompClient;
let gameId;


function connectToSocket(endpoint) {
  console.log("connecting to the game");
  let socket = new SockJS('http://localhost:8080/' + endpoint + "/");
  stompClient = Stomp.over(socket);
  stompClient.connect({}, function (frame) {
      console.log("connected to the frame: "+frame);
      stompClient.subscribe("/topic/" + endpoint + "/", function (response) {
        let data = JSON.parse(response.body);
        console.log(data);
      })
  })
}

function connectToGame(gameId) {
    console.log("connecting to the game");
    let socket = new SockJS('http://localhost:8080/gameplay');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log("connected to the frame: "+frame);
        stompClient.subscribe("/topic/gameplay/" + gameId, function (response) {
            let data = JSON.parse(response.body);
            console.log(data);
        })
    })
}

function create_game() {
  let login = "adam-1";
  axios.post("http://localhost:8080/game/start", {"login": login}).then(res => {
      console.log("gameId: " +res.data.gameId);
      gameId = res.data.gameId;
      connectToGame(gameId);
    });
}

function connectToRandom() {
  let login = "adam-2";
  axios.post("http://localhost:8080/game/connect/random", {"login": login}).then(res => {
      console.log("gameId: " +res.data.gameId);
      gameId = res.data.gameId
      connectToGame(gameId);
      console.log("Playing with: " + res.data.player1.login);
    });
}

function addUnit() {
  axios.post("http://localhost:8080/game/gameplay", {"unit":{"name": "nazwa", "owner": "adamto", "hp": 10}, "gameId": gameId}).then(res => {
      console.log("added unit to game with an ID of: " + res.data.gameId)

  });
}

window.onload = function () {
    connectToSocket("rooms")
}

function App() {
  return (
    <div className="App">
        <button onMouseUp={create_game}>Create</button>
        <button onMouseUp={connectToRandom}>Join</button>
        <button onMouseUp={addUnit}>Add Unit</button>
    </div>
  );
}

export default App;
